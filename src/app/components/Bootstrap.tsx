import {routerMiddleware as createRouterMiddleware, ConnectedRouter} from 'connected-react-router';
import {createBrowserHistory} from 'history';
import React from 'react';
import {Provider as StoreProvider} from 'react-redux';
import {applyMiddleware, createStore, AnyAction, Store} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import {RootState} from '../models/RootState';
import {createReducer} from '../reducers/root';
import {createSaga} from '../sagas/root';
import {App} from './App';

const history = createBrowserHistory();
const sagaMiddleware = createSagaMiddleware();
const routerMiddleware = createRouterMiddleware(history);

function configureStore(): Store<RootState, AnyAction> {
  return createStore(createReducer(history), composeWithDevTools(applyMiddleware(routerMiddleware, sagaMiddleware)));
}

const store = configureStore();

sagaMiddleware.run(createSaga());

export const Bootstrap: React.FunctionComponent = (): React.ReactElement => (
  <StoreProvider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </StoreProvider>
);
