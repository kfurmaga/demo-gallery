import React, {Component, ReactElement} from 'react';
import {Route, Switch} from 'react-router';
import {GalleriesList} from '../containers/GalleriesList';
import {Gallery} from '../containers/Gallery';

export class App extends Component {
  public render(): ReactElement {
    return (
      <div>
        <main>
          <Switch>
            <Route path="/gallery" component={Gallery} />
            <Route path="/" component={GalleriesList} />
          </Switch>
        </main>
      </div>
    );
  }
}
