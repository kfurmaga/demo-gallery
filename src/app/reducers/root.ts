import {connectRouter} from 'connected-react-router';
import {History} from 'history';
import {combineReducers, Reducer} from 'redux';
import {reducer as galleriesReducer} from '../../gallery/reducers/galleries';
import {reducer as galleryReducer} from '../../gallery/reducers/gallery';
import {RootState} from '../models/RootState';

export const createReducer = (history: History): Reducer<RootState> =>
  combineReducers<RootState>({
    router: connectRouter(history),
    gallery: galleryReducer,
    galleries: galleriesReducer,
  });
