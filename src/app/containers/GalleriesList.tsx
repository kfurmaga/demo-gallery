import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {show} from '../../gallery/actions/GalleriesAction';
import {GalleriesList as Component, Props as ComponentProps} from '../../gallery/components/GalleriesList';
import {GalleryListItem} from '../../gallery/models/GalleryListItem';
import {RootState} from '../models/RootState';
import {getList} from '../selectors/galleries';

export interface InputProps {}

export interface StateMappedProps extends Pick<ComponentProps, 'galleries'> {}

export interface DispatchMappedProps extends Pick<ComponentProps, 'onClick'> {}

export function mapStateToProps(state: RootState): StateMappedProps {
  return {
    galleries: getList(state),
};
}

export function mapDispatchToProps(dispatch: Dispatch): DispatchMappedProps {
  return {
    onClick: (item: GalleryListItem) => dispatch(show(item))
  };
}
export const GalleriesList = connect<StateMappedProps, DispatchMappedProps, InputProps, RootState>(
  mapStateToProps,
  mapDispatchToProps
)(Component);
