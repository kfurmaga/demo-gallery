import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {nextPage, previousPage, selectPhoto} from '../../gallery/actions/GalleryAction';
import {Gallery as Component, Props as GalleryProps} from '../../gallery/components/Gallery';
import {Gallery as Model} from '../../gallery/models/Gallery';
import {Photo} from '../../gallery/models/Photo';
import {RootState} from '../models/RootState';
import {getGallery} from '../selectors/gallery';

export interface InputProps {}

export interface StateMappedProps extends Pick<GalleryProps, 'photos' | 'current' | 'isNextVisible' | 'isPreviousVisible'> {}

export interface DispatchMappedProps extends Pick<GalleryProps, 'onClick' | 'onNextPage' | 'onPreviousPage'> {}

export function mapStateToProps(state: RootState): StateMappedProps {
  const gallery: Model = getGallery(state);
  return {
    photos: gallery.photos,
    current: gallery.current,
    isNextVisible: gallery.page < gallery.totalPages - 1,
    isPreviousVisible: gallery.page > 0,
  };
}

export function mapDispatchToProps(dispatch: Dispatch): DispatchMappedProps {
  return {
    onClick: (photo: Photo) => dispatch(selectPhoto(photo)),
    onNextPage: () => dispatch(nextPage()),
    onPreviousPage: () => dispatch(previousPage())
  };
}
export const Gallery = connect<StateMappedProps, DispatchMappedProps, InputProps, RootState>(
  mapStateToProps,
  mapDispatchToProps
)(Component);
