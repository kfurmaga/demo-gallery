import {Saga, SagaIterator} from 'redux-saga';
import {all, call, put, select, takeEvery} from 'redux-saga/effects';
import {takeRoute} from '../../common/sagas/routing';
import {setPage, setPhotos, update, Type as GalleryAction} from '../../gallery/actions/GalleryAction';
import {GalleryClient} from '../../gallery/clients/GalleryClient';
import {Gallery} from '../../gallery/models/Gallery';
import {Photo} from '../../gallery/models/Photo';
import {getGallery} from '../selectors/gallery';

interface GalleryPathParams {
  galleryId: string;
}

export function createSaga(client: GalleryClient): Saga {
  function* load({galleryId}: GalleryPathParams): SagaIterator {
    const gallery: Gallery = yield call(client.getGallery, +galleryId);
    yield put(update(gallery));
  }

  function* loadNextPage(): SagaIterator {
    const gallery: Gallery = yield select(getGallery);

    if (gallery.page < gallery.totalPages - 1) {
      const page: number = gallery.page + 1;
      const photos: Photo[] = yield call(client.getPhotos, gallery.id, page);

      yield put(setPage(page));
      yield put(setPhotos(photos));
    }
  }

  function* loadPreviousPage(): SagaIterator {
    const gallery: Gallery = yield select(getGallery);
    if (gallery.page > 0) {
      const page: number = gallery.page - 1;
      const photos: Photo[] = yield call(client.getPhotos, gallery.id, page);

      yield put(setPage(page));
      yield put(setPhotos(photos));
    }
  }

  return function* saga(): SagaIterator {
    yield all([
      takeRoute<GalleryPathParams>('/gallery/:galleryId', load),
      takeEvery(GalleryAction.nextPage, loadNextPage),
      takeEvery(GalleryAction.previousPage, loadPreviousPage),
    ]);
  };
}
