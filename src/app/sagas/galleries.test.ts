import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {push, LOCATION_CHANGE} from 'connected-react-router';
import {createMemoryHistory, History} from 'history';
import {expectSaga, ExpectApi} from 'redux-saga-test-plan';
import {generate} from '../../common/helpers/array';
import {show, update, Type} from '../../gallery/actions/GalleriesAction';
import {GalleryClient} from '../../gallery/clients/GalleryClient';
import {createGalleryClient, getUrl} from '../../gallery/clients/GalleryClient.mock';
import {GalleryListItem} from '../../gallery/models/GalleryListItem';
import {createGalleryListItem} from '../../gallery/models/GalleryListItem.mock';
import {createReducer} from '../reducers/root';
import {createSaga} from './galleries';

describe('app.sagas.galleries', () => {
  const client: GalleryClient = createGalleryClient();
  const mockAxios: MockAdapter = new MockAdapter(axios);
  let saga: any;
  let sut: ExpectApi;

  beforeEach(() => {
    const history: History = createMemoryHistory();
    saga = createSaga(client);
    sut = expectSaga(saga).withReducer(createReducer(history));
  });

  afterEach(() => {
    mockAxios.reset();
  });

  describe('GalleriesAction.show(item)', () => {
    const expectedListItem: GalleryListItem = createGalleryListItem(333);

    beforeEach(() => {
      sut = sut.dispatch(show(expectedListItem));
    });

    it('redirects to gallery page', async () => {
      await sut.put(push('/gallery/333')).silentRun();
    });
  });

  describe('@@router/LOCATION_CHANGE - /', () => {
    const galleries: GalleryListItem[] = generate(10).map(createGalleryListItem);

    beforeEach(() => {
      sut = sut.dispatch({
        type: LOCATION_CHANGE,
        payload: {
          location: {
            pathname: '/',
            search: '',
            hash: '',
            state: {},
          },
        },
      });
    });

    it('loads list of galleries', async () => {
      mockAxios.onGet(getUrl('/albums')).reply(200, galleries);

      await sut.put(update(galleries)).silentRun();
    });
  });
});
