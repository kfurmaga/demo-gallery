import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {LOCATION_CHANGE} from 'connected-react-router';
import {createMemoryHistory, History} from 'history';
import {expectSaga, ExpectApi} from 'redux-saga-test-plan';
import {generate} from '../../common/helpers/array';
import {nextPage, previousPage, setPhotos, update} from '../../gallery/actions/GalleryAction';
import {GalleryClient} from '../../gallery/clients/GalleryClient';
import {createGalleryClient, getUrl, pageSize} from '../../gallery/clients/GalleryClient.mock';
import {Gallery} from '../../gallery/models/Gallery';
import {createGallery} from '../../gallery/models/Gallery.mock';
import {Photo} from '../../gallery/models/Photo';
import {createPhoto} from '../../gallery/models/Photo.mock';
import {initialState} from '../../gallery/reducers/gallery';
import {createReducer} from '../reducers/root';
import {createSaga} from './gallery';

describe('app.sagas.gallery', () => {
  const client: GalleryClient = createGalleryClient();
  let mockAxios: MockAdapter;
  let saga: any;
  let sut: ExpectApi;

  beforeEach(() => {
    const history: History = createMemoryHistory();
    saga = createSaga(client);
    sut = expectSaga(saga);
    mockAxios = new MockAdapter(axios);
  });

  afterEach(() => {
    mockAxios.reset();
  });

  describe('@@router/LOCATION_CHANGE - /gallery/333', () => {
    const totalPages: number = 20;
    const requestedPhotos: Photo[] = generate(pageSize * totalPages).map(createPhoto);
    const expectedPhotos: Photo[] = requestedPhotos.slice(0, pageSize);
    const expectedGallery: Gallery = {
      ...createGallery(333),
      photos: expectedPhotos,
      current: expectedPhotos[0],
      totalPages: totalPages
    };

    beforeEach(() => {
      sut = sut.dispatch({
        type: LOCATION_CHANGE,
        payload: {
          location: {
            pathname: '/gallery/333',
            search: '',
            hash: '',
            state: {},
          },
        },
      });
    });

    it('loads gallery', async () => {
      mockAxios.onGet(getUrl(`/photos?albumId=333`)).reply(200, requestedPhotos);

      await sut.put(update(expectedGallery)).silentRun();
    });
  });

  describe('GalleryAction.nextPage', () => {
    const expectedPhotos: Photo[] = generate(pageSize).map(createPhoto);
    const id: number = 333;
    const currentPage: number = 4;

    it('loads next page', async () => {
      sut = sut
        .withState({
          gallery: {
            ...initialState,
            id,
            page: currentPage,
            totalPages: currentPage + 2,
          }
        })
        .dispatch(nextPage());
      mockAxios.onGet(getUrl(`/photos?albumId=${id}&_start=${(currentPage + 1) * pageSize}&_limit=${pageSize}`)).reply(200, expectedPhotos);

      await sut.put(setPhotos(expectedPhotos)).silentRun();
    });

    it('doesn\'t load previous page when page is last page', async () => {
      sut = sut
        .withState({
          gallery: {
            ...initialState,
            id,
            page: currentPage,
            totalPages: currentPage
          }
        })
        .dispatch(nextPage());

      await sut.not.put(setPhotos(expectedPhotos)).silentRun();
    });
  });

  describe('GalleryAction.previousPage', () => {
    const expectedPhotos: Photo[] = generate(pageSize).map(createPhoto);
    const id: number = 333;
    const currentPage: number = 4;

    it('loads previous page', async () => {
      sut = sut
        .withState({
          gallery: {
            ...initialState,
            id,
            page: currentPage
          }
        })
        .dispatch(previousPage());

      mockAxios.onGet(getUrl(`/photos?albumId=${id}&_start=${(currentPage - 1) * pageSize}&_limit=${pageSize}`)).reply(200, expectedPhotos);

      await sut.put(setPhotos(expectedPhotos)).silentRun();
    });

    it('doesn\'t load previous page when page is first page', async () => {
      sut = sut
        .withState({
          gallery: {
            ...initialState,
            id,
            page: 0
          }
        })
        .dispatch(previousPage());

      await sut.not.put(setPhotos(expectedPhotos)).silentRun();
    });
  });
});
