import {History} from 'history';
import {SagaIterator} from 'redux-saga';
import {all, fork} from 'redux-saga/effects';
import {GalleryClient} from '../../gallery/clients/GalleryClient';
import {createSaga as createGalleriesSaga} from './galleries';
import {createSaga as createGallerySaga} from './gallery';

export function createSaga(): () => SagaIterator {
  const galleryClient: GalleryClient = new GalleryClient('https://jsonplaceholder.typicode.com', 10);

  return function* rootSaga(): SagaIterator {
    yield all([fork(createGalleriesSaga(galleryClient)), fork(createGallerySaga(galleryClient))]);
  };
}
