import {push} from 'connected-react-router';
import {Action} from 'redux-actions';
import {Saga, SagaIterator} from 'redux-saga';
import {all, call, put, takeEvery} from 'redux-saga/effects';
import {takeRoute} from '../../common/sagas/routing';
import {update, Type as GalleriesAction} from '../../gallery/actions/GalleriesAction';
import {GalleryClient} from '../../gallery/clients/GalleryClient';
import {GalleryListItem} from '../../gallery/models/GalleryListItem';

export function createSaga(client: GalleryClient): Saga {
  function* load(): SagaIterator {
    const galleries: GalleryListItem[] = yield call(client.getGalleries);
    yield put(update(galleries));
  }

  function* showGallery(action: Action<GalleryListItem>): SagaIterator {
    yield put(push(`/gallery/${action.payload!.id}`));
  }

  return function* saga(): SagaIterator {
    yield all([
      takeRoute<void>('/', load),
      takeEvery(GalleriesAction.show, showGallery)
    ]);
  };
}
