import {GalleryListItem} from '../../gallery/models/GalleryListItem';
import {RootState} from '../models/RootState';

export function getList(state: RootState): GalleryListItem[] {
  return state.galleries;
}
