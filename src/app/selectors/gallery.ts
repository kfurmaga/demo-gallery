import {Gallery} from '../../gallery/models/Gallery';
import {RootState} from '../models/RootState';

export function getGallery(state: RootState): Gallery {
  return state.gallery;
}
