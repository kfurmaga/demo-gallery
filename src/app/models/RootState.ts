import {RouterState} from 'connected-react-router';
import {Gallery} from '../../gallery/models/Gallery';
import {GalleryListItem} from '../../gallery/models/GalleryListItem';

export interface RootState {
  readonly router: RouterState;
  gallery: Gallery;
  galleries: GalleryListItem[];
}
