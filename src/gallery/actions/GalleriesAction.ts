import {createAction} from 'redux-actions';
import {GalleryListItem} from '../models/GalleryListItem';

export enum Type {
  show = 'gallery.GalleriesAction.show',
  load = 'gallery.GalleriesAction.load',
  update = 'gallery.GalleriesAction.update',
}

export const load = createAction(Type.load);
export const update = createAction<GalleryListItem[]>(Type.update);
export const show = createAction<GalleryListItem>(Type.show);
