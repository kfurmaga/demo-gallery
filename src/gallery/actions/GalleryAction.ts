import {createAction} from 'redux-actions';
import {Gallery} from '../models/Gallery';
import {Photo} from '../models/Photo';

export enum Type {
  update = 'gallery.GalleryAction.update',
  setPhotos = 'gallery.GalleryAction.setPhotos',
  selectPhoto = 'gallery.GalleryAction.selectPhoto',
  setPage = 'gallery.GalleryAction.setPage',
  nextPage = 'gallery.GalleryAction.nextPage',
  previousPage = 'gallery.GalleryAction.previousPage',
}

export const update = createAction<Gallery>(Type.update);
export const setPhotos = createAction<Photo[]>(Type.setPhotos);
export const selectPhoto = createAction<Photo>(Type.selectPhoto);
export const setPage = createAction<number>(Type.setPage);
export const nextPage = createAction(Type.nextPage);
export const previousPage = createAction(Type.previousPage);
