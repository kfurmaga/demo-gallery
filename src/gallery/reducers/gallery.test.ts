import {generate} from '../../common/helpers/array';
import {selectPhoto, setPage, setPhotos, update} from '../actions/GalleryAction';
import {Gallery} from '../models/Gallery';
import {createGallery} from '../models/Gallery.mock';
import {Photo} from '../models/Photo';
import {createPhoto} from '../models/Photo.mock';
import {initialState, reducer} from './gallery';

describe('gallery.reducers.gallery', () => {
  describe('GalleryAction.update', () => {
    it('updates state', () => {
      const expectedGallery: Gallery = createGallery(333);
      const result: Gallery = reducer(initialState, update(expectedGallery));
      expect(result).toEqual(expectedGallery);
    });
  });

  describe('GalleryAction.setPhotos', () => {
    it('updates photos', () => {
      const expectedImages: Photo[] = generate(10).map(createPhoto);
      const result: Gallery = reducer(initialState, setPhotos(expectedImages));
      expect(result.photos).toEqual(expectedImages);
    });

    it("doesn't update current when new photos were update", () => {
      const images: Photo[] = generate(10).map(createPhoto);
      const state: Gallery = {
        ...initialState,
        photos: images,
        current: images[4],
      };
      const newImages: Photo[] = generate(10)
        .map((i: number) => i + 100)
        .map(createPhoto);
      const result: Gallery = reducer(state, setPhotos(newImages));
      expect(result.current).toBe(state.current);
    });
  });

  describe('GalleryAction.selectPhoto', () => {
    it('updates current', () => {
      const expectedImage: Photo = createPhoto(333);
      const result: Gallery = reducer(initialState, selectPhoto(expectedImage));
      expect(result.current).toEqual(expectedImage);
    });
  });

  describe('GalleryAction.setPage', () => {
    it('updates page', () => {
      const expectedPage: number = 333;
      const result: Gallery = reducer(initialState, setPage(expectedPage));
      expect(result.page).toEqual(expectedPage);
    });
  });
});
