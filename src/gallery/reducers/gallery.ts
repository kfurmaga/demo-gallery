import {handleActions} from 'redux-actions';
import {Type} from '../actions/GalleryAction';
import {Gallery} from '../models/Gallery';
import {Photo} from '../models/Photo';

export const initialState: Gallery = {
  id: 0,
  photos: [],
  current: null,
  page: 0,
  totalPages: 0,
};

export const reducer = handleActions<Gallery, Gallery | Photo[] | Photo | number>(
  {
    [Type.update]: (state, action) => action.payload! as Gallery,
    [Type.setPhotos]: (state, action) => ({
      ...state,
      photos: action.payload! as Photo[],
    }),
    [Type.selectPhoto]: (state, action) => ({
      ...state,
      current: action.payload! as Photo,
    }),
    [Type.setPage]: (state, action) => ({
      ...state,
      page: action.payload! as number,
    }),
  },
  initialState
);
