import {handleActions} from 'redux-actions';
import {Type} from '../actions/GalleriesAction';
import {GalleryListItem} from '../models/GalleryListItem';

export const initialState: GalleryListItem[] = [];

export const reducer = handleActions<GalleryListItem[], GalleryListItem[]>(
  {
    [Type.update]: (state, action) => action.payload! as GalleryListItem[],
  },
  initialState
);
