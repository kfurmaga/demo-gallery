import {generate} from '../../common/helpers/array';
import {update} from '../actions/GalleriesAction';
import {GalleryListItem} from '../models/GalleryListItem';
import {createGalleryListItem} from '../models/GalleryListItem.mock';
import {initialState, reducer} from './galleries';

describe('gallery.reducers.galleries', () => {
  describe('GalleriesAction.update', () => {
    it('updates state', () => {
      const expectedGalleries: GalleryListItem[] = generate(10).map(createGalleryListItem);
      const result: GalleryListItem[] = reducer(initialState, update(expectedGalleries));
      expect(result).toEqual(expectedGalleries);
    });
  });
});
