import {GalleryListItem} from './GalleryListItem';

export function createGalleryListItem(id: number): GalleryListItem {
  return {
    id: id,
    title: `${id}.GalleryListItem.title`
  };
}
