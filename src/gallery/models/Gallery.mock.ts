import {generate} from '../../common/helpers/array';
import {Gallery} from './Gallery';
import {Photo} from './Photo';
import {createPhoto} from './Photo.mock';

export function createGallery(id: number): Gallery {
  const photos: Photo[] = generate(10).map(i => id * 1000 + i).map(createPhoto);
  return {
    id: id,
    photos: photos,
    current: photos[0],
    page: 0,
    totalPages: 100,
  };
}
