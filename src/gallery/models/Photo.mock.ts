import {Photo} from './Photo';

export function createPhoto(id: number): Photo {
  return {
    id: id,
    title: `${id}.Photo.title`,
    url: `${id}.Photo.url`,
    thumbnailUrl: `${id}.Photo.url`
  };
}
