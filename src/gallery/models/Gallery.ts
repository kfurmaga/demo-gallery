import {Photo} from './Photo';

export interface Gallery {
  id: number;
  photos: Photo[];
  current: Photo | null;
  page: number;
  totalPages: number;
}
