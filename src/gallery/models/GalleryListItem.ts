export interface GalleryListItem {
  id: number;
  title: string;
}
