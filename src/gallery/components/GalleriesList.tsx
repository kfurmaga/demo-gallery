import React, {Component, ReactElement} from 'react';
import {GalleryListItem} from '../models/GalleryListItem';

export interface Props {
  galleries: GalleryListItem[];
  onClick: (item: GalleryListItem) => void;
}

export class GalleriesList extends Component<Props> {
  private readonly renderItem = (item: GalleryListItem): ReactElement => {
    const {onClick} = this.props;

    return (
      <a key={item.id} onClick={() => onClick(item)}>
        <p>{item.title}</p>
      </a>
    );
  };

  public render(): ReactElement {
    const {galleries} = this.props;

    return <div>galleries: {galleries.map(this.renderItem)}</div>;
  }
}
