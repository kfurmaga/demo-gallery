import React, {Component, ReactElement} from 'react';
import {Photo} from '../models/Photo';

export interface Props {
  current: Photo | null;
  photos: Photo[];
  isNextVisible: boolean;
  isPreviousVisible: boolean;
  onClick: (photo: Photo) => void;
  onNextPage: () => void;
  onPreviousPage: () => void;
}

export class Gallery extends Component<Props> {
  private readonly renderThumbnail = (photo: Photo): ReactElement => {
    const {onClick} = this.props;

    return (
      <div key={photo.id}>
        <img src={photo.thumbnailUrl} alt={photo.title} onClick={() => onClick(photo)} />
      </div>
    );
  };

  public render(): ReactElement {
    const {photos, current, isNextVisible, isPreviousVisible, onNextPage, onPreviousPage} = this.props;

    return (
      <div>
        <div>{current && <img src={current.url} alt={current.title} />}</div>
        {isPreviousVisible && <a onClick={onPreviousPage}>prev</a>}
        {isNextVisible && <a onClick={onNextPage}>next</a>}
        {photos.map(this.renderThumbnail)}
      </div>
    );
  }
}
