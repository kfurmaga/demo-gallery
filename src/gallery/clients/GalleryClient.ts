import axios, {AxiosPromise} from 'axios';
import {Gallery} from '../models/Gallery';
import {GalleryListItem} from '../models/GalleryListItem';
import {Photo} from '../models/Photo';

export class GalleryClient {
  public constructor(private readonly _apiUrl: string, private readonly _pageSize: number) {}

  public readonly getGalleries = (): Promise<GalleryListItem[]> => {
    return axios.get<GalleryListItem[]>(this.getUrl('/albums')).then((response) => response.data);
  };

  public readonly getGallery = (id: number): Promise<Gallery> => {
    return axios.get<Photo[]>(this.getUrl(`/photos?albumId=${id}`)).then((response) => ({
      id: id,
      photos: response.data.slice(0, this._pageSize),
      current: response.data[0] || null,
      page: 0,
      totalPages: Math.ceil(response.data.length / this._pageSize),
    }));
  };

  public readonly getPhotos = (galleryId: number, page: number): Promise<Photo[]> => {
    const start: number = this._pageSize * page;
    return axios
      .get<Photo[]>(this.getUrl(`/photos?albumId=${galleryId}&_start=${start}&_limit=${this._pageSize}`))
      .then((response) => response.data);
  };

  private getUrl(path: string): string {
    return `${this._apiUrl}${path}`;
  }
}
