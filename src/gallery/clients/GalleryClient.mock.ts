import {GalleryClient} from './GalleryClient';

export const host: string = 'test://mock';
export const pageSize: number = 5;

export function getUrl(path: string): string {
  return `${host}${path}`;
}

export function createGalleryClient(): GalleryClient {
  return new GalleryClient(
    host,
    pageSize
  );
}
