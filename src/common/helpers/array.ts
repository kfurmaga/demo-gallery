export function generate(length: number): number[] {
  return Array.from(Array(length).keys());
}
