import {LOCATION_CHANGE} from 'connected-react-router';
import {match as Match, matchPath} from 'react-router';
import {Task} from 'redux-saga';
import {cancel, fork, take, ForkEffect} from 'redux-saga/effects';

export const takeRoute = <MatchParams>(path: string, worker: (match: MatchParams) => void): ForkEffect =>
  fork(function* takeRouteSaga() {
    while (true) {
      let task: Task | null = null;

      const action = yield take(LOCATION_CHANGE);

      const match = matchPath<MatchParams>(action.payload!.location.pathname, {
        path,
        exact: true,
      });

      if (task) {
        yield cancel(task);
      }

      if (match && match.isExact) {
        task = yield fork(worker, match.params);
      }
    }
  });
